ASM=nasm
ASMFLAGS=-f elf64

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

main.o: main.asm
	$(ASM) $(ASMFLAGS) -o main.o main.asm

program: lib.o main.o dict.o
	ld -o program lib.o main.o dict.o
