section .data
	%include 'colon.inc'
	%include 'words.inc'
	wor: times 255 db 0xca
	message: db 10,'Long string', 10, 0
	primer: db 'third word', 0
	messageNotFind: db 10, 'Not Found', 0
section .text
global _start
extern find_word
extern string_length
extern read_char
	_start:
		mov rsi, 255
		mov rdi, wor
		call read_string
		test rax, rax
		jz .errorSize
		mov rsi, liza
		mov rdi, wor
		call find_word
		test rax, rax
		jnz .allOk
	.errorFind:
		mov rax, 1
		mov rdi, 2
		mov rsi, messageNotFind
		mov rdx, 10
		syscall
		jmp .exit
	.errorSize:
		mov rax, 1
		mov rdi, 2
		mov rsi, message
		mov rdx, 13
		syscall
		jmp .exit
	.allOk:
		mov rdi, rax
		add rdi, 8
		call string_length
		add rdi, rax
		inc rdi
		mov rsi, rdi
		call string_length
		mov rdx, rax
		mov rax, 1
		mov rdi, 1
		syscall
	.exit:
		mov rax, 60
		xor rdi, rdi
		syscall
	read_string:
		xor rax, rax
		push rbx
		push r8
		xor r8, r8
		mov rbx, rsi
		.string:
			push rdi
			call read_char
			pop rdi
			mov byte[rdi+r8], al
			cmp al, 0
			jz .end
			cmp al, 10
			jz .end
			inc r8
			cmp r8, rbx
			je .error
			jmp .string
		.end:
			mov byte[rdi+r8], 0
			mov rax, r8
			pop r8
			pop rbx
			ret
		.error:
			xor rax, rax
			pop r8
			pop rbx
			ret
