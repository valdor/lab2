extern string_equals
global find_word

section .text
	find_word:
		xor rax, rax
		jmp .loop
	.newWord:
		sub rsi,8
		mov r8, [rsi]
		test r8, r8
		je .noEqualEnd
		mov rsi, [rsi]
	.loop:
		add rsi, 8
		call string_equals
		test rax, rax
		jz .newWord
		jnz .findWord
	.noEqualEnd:
		mov rax, 0
		ret
	.findWord:
		sub rsi, 8
		mov rax, rsi
		ret
